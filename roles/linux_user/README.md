Role Linux User
=========

Manages user and group accounts.

Requirements
------------

Role needs to be run as privledged user.

Role Variables
--------------

#TODO

Dependencies
------------

None

Example Playbook
----------------


    - hosts: host
      become: yes
      gather_facts: no
      strategy: free

      vars_files:
        - ../vault_file.yml

      roles:
        - role: linux_user
          vars:
            groups_to_create:
              - group_create: test1
              - group_create: test2

            users_to_create:
                - {user_create: 'test1', user_password_create: 'test', user_add_to_group: [test1,test2], user_comment: 'Try comment     1'}
                - {user_create: 'test2'}

            groups_to_remove: 
                - group_remove: test1
                - group_remove: test2

            users_to_remove:
                - user_remove: test1
                - user_remove: test2

License
-------

Apache

Author Information
------------------

maksym.sereda@hotmail.com
